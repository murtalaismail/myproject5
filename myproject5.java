/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foreverloop;

/**
 *
 * @author user
 */
public class ForEverLoop {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       // This program tests Forever Loop with a Sentinel.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        long sum=0; int count=0; double average;
        for(;true;)
        {
            System.out.print("Enter a number: ");
            int num = sc.nextInt();
            if(num==0) break;
            sum = sum + num;
            count = count + 1;
        }
        
        if(count==0)
        {
            System.out.println("You didn't enter any number");
        }
        else
        {
            average = (double)sum/count;
            System.out.println("The sum of the numbers = "+sum);
            System.out.println("The average of the numbers = "+average);
        } 
    }
}